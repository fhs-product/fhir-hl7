package com.vhades.fhir.fhirhl7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

@ServletComponentScan
@SpringBootApplication
@ComponentScan(basePackages = "com.vhades.fhir.fhirhl7.*")
public class FhirHl7Application {

	public static void main(String[] args) {
		SpringApplication.run(FhirHl7Application.class, args);
	}

}
