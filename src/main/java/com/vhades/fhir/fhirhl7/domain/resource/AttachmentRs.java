package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AttachmentRs implements Serializable {
	protected String contentType;
	protected String language;
	protected byte[] data;
	protected String url;
	protected int size;
	protected byte[] hash;
	protected String title;
	protected LocalDate creation;
}
