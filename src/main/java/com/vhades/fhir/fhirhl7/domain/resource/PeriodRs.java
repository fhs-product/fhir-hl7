package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PeriodRs implements Serializable {

	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate start;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate end;
}
