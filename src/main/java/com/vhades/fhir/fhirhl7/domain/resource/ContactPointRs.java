package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ContactPointRs implements Serializable {
	private String system;
	private String value;
	private String use;
	private int rank;
	private PeriodRs period;
}
