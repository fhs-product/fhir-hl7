package com.vhades.fhir.fhirhl7.domain;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Builder
public class HistoryEntityId implements Serializable {
	
	private UUID id;
	private Long version;
}
