package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AddressRs implements Serializable {
	private String use;
	protected String type;
	protected String text;
	protected List<String> line;
	protected String city;
	protected String district;
	protected String state;
	protected String postalCode;
	protected String country;
	private PeriodRs period;
}
