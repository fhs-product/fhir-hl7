package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@SuperBuilder(toBuilder = true)
@ToString
public class PatientRs extends DomainResource implements Serializable {
	private List<IdentifierRs> identifier;
	private boolean active;
	private List<HumanNameRs> name;
	private List<ContactPointRs> telecom;
	private String gender;
	private LocalDate birthDate;
	// deceased[x]: Indicates if the individual is deceased or not. One of these 2:
	private Boolean deceasedBoolean;
	private LocalDateTime deceasedDateTime;
	
	private List<AddressRs> address;
	private CodeableConceptRs maritalStatus;
	// multipleBirth[x]: Whether patient is part of a multiple birth. One of these 2:
	private Boolean multipleBirthBoolean;
	private Integer multipleBirthInteger;
	
	private List<AttachmentRs> photo;
	private List<ContactRs> contact;
	private List<PatientCommunicationRs> communication;
	
	private List<ReferenceRs> generalPractitioner;
	private ReferenceRs managingOrganization;
	
	private List<PatientLinkRs> link;
}
