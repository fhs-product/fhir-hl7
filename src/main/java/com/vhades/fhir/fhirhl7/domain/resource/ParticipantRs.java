package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ParticipantRs implements Serializable {
	private List<CodeableConceptRs> type;
	private ReferenceRs actor;
	private String required;
	private String status;
	private PeriodRs period;

}
