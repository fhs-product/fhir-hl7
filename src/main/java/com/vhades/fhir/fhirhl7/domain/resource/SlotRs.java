package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@SuperBuilder(toBuilder = true)
@ToString
public class SlotRs extends DomainResource implements Serializable {
	private List<IdentifierRs> identifier;
	private List<CodeableConceptRs> serviceCategory;
	private List<CodeableConceptRs> serviceType;
	private List<CodeableConceptRs> specialty;
	private CodeableConceptRs appointmentType;
	private ReferenceRs schedule;
	private String status;
	private LocalDate start;
	private LocalDate end;
	private boolean overbooked;
	private String comment;
}
