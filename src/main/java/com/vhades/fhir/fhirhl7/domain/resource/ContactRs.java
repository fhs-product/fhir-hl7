package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ContactRs implements Serializable {
	protected List<CodeableConceptRs> relationship;
	protected HumanNameRs name;
	protected List<ContactPointRs> telecom;
	protected AddressRs address;
	protected String gender;
	protected ReferenceRs organization;
	private PeriodRs period;
}
