package com.vhades.fhir.fhirhl7.domain.resource;

import com.vhades.fhir.fhirhl7.domain.resource.validation.ResourceValidator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@SuperBuilder(toBuilder = true)
@ToString
public class AppointmentRs extends DomainResource implements Serializable {
	private List<IdentifierRs> identifier;
	private String status;
	private CodeableConceptRs cancelationReason;
	private List<CodeableConceptRs> serviceCategory;
	private List<CodeableConceptRs> serviceType;
	private List<CodeableConceptRs> specialty;
	private CodeableConceptRs appointmentType;
	private List<CodeableConceptRs> reasonCode;
	private int priority;
	private String description;
	private int minutesDuration;
	private List<ReferenceRs> slot;
	private LocalDate created;
	private String comment;
	private List<ParticipantRs> participant;
	private List<PeriodRs> requestedPeriod;

	private static final class AppointmentRsBuilderImpl extends AppointmentRs.AppointmentRsBuilder<AppointmentRs, AppointmentRsBuilderImpl> {
		@Override
		public AppointmentRs build() {
			AppointmentRsBuilder<?, ?> builder = this;

			ResourceValidator.execute(new ResourceValidator[] {
					new ResourceValidator(() -> builder.status == null, () -> "No status supplied"),
					new ResourceValidator(() -> builder.slot == null || builder.slot.isEmpty(), () -> "At least one slot is required"),
					new ResourceValidator(() -> builder.participant == null || builder.participant.isEmpty(), () -> "At least one participant is required")
					//...
			});

			return new AppointmentRs(this);
		}
	}

}
