package com.vhades.fhir.fhirhl7.domain;


import com.vhades.fhir.fhirhl7.domain.resource.PatientRs;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "patient")
public class PatientEntity extends BaseEntity {

	@Id @Type(type = "pg-uuid")
	private UUID id;

	@Builder.Default
	private LocalDateTime createdDate = LocalDateTime.now();

	@Builder.Default
	private LocalDateTime modifiedDate = LocalDateTime.now();

	private String createdBy;

	private String modifiedBy;

	@Version
	@Builder.Default
	private Long version = 1L;

	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private PatientRs resource;
}
