package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class HumanNameRs implements Serializable {
	private String use;
	private String text;
	private String family;
	private List<String> given;
	private List<String> prefix;
	private List<String> suffix;
	private PeriodRs period;
}
