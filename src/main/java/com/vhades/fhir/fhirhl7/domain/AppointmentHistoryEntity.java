package com.vhades.fhir.fhirhl7.domain;

import com.vhades.fhir.fhirhl7.domain.resource.AppointmentRs;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "appointment_history")
public class AppointmentHistoryEntity extends BaseEntity {
	
	@EmbeddedId
    private HistoryEntityId id;

	@Builder.Default
	private LocalDateTime createdDate = LocalDateTime.now();

	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
	private AppointmentRs resource;
	
}
