package com.vhades.fhir.fhirhl7.domain.resource;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(toBuilder = true)
public class MetaRs implements Serializable {

	@Builder.Default
	protected Long version = 1L;
	@Builder.Default
	protected LocalDateTime lastUpdated = LocalDateTime.now();
	protected String source;
	protected List<CodingRs> security;
	protected List<CodingRs> tag;
}
