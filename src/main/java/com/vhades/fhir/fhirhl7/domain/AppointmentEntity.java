package com.vhades.fhir.fhirhl7.domain;

import com.vhades.fhir.fhirhl7.domain.resource.AppointmentRs;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "appointment")
public class AppointmentEntity extends BaseEntity {

	@Id @Type(type = "pg-uuid")
	private UUID id;

	@Builder.Default
	private LocalDateTime createdDate = LocalDateTime.now();

	@Builder.Default
	private LocalDateTime modifiedDate = LocalDateTime.now();

	private String createdBy;

	private String modifiedBy;

	@Version
	@Builder.Default
	private Long version = 1L;

	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private AppointmentRs resource;

	//	public static class AppointmentEntityBuilder {
	//		private AppointmentRs resource;
	//
	//		// custom setter for resource: primary key and resource.id should be the same
	//		public AppointmentEntityBuilder resource(AppointmentRs resource) {
	//			if (this.id == null) { // in case of updating, no need to set the id
	//				this.id(resource.getId());
	//			}
	//			this.resource = resource;
	//			return this;
	//		}
	//	}
}
