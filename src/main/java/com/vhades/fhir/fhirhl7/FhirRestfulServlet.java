package com.vhades.fhir.fhirhl7;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.interceptor.LoggingInterceptor;
import com.vhades.fhir.fhirhl7.controller.resource.provider.PatientResourceProvider;
import com.vhades.fhir.fhirhl7.interceptor.BusinessExceptionInterceptor;
import com.vhades.fhir.fhirhl7.controller.resource.provider.AppointmentResourceProvider;
import com.vhades.fhir.fhirhl7.controller.resource.provider.SlotResourceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.util.Arrays;

@Configuration
@WebServlet(urlPatterns = { "/fhir/*" }, displayName = "FHIR Server")
public class FhirRestfulServlet extends RestfulServer {
	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;

	@Override
	protected void initialize() throws ServletException {

		// create a context for the appropriate version
		setFhirContext(FhirContext.forR4());

		setResourceProviders(Arrays.asList(
				applicationContext.getBean(PatientResourceProvider.class),
				applicationContext.getBean(SlotResourceProvider.class),
				applicationContext.getBean(AppointmentResourceProvider.class)
				));

		// Now register the logging interceptor
		LoggingInterceptor loggingInterceptor = new LoggingInterceptor();
		registerInterceptor(loggingInterceptor);

		// The SLF4j logger "test.accesslog" will receive the logging events
		loggingInterceptor.setLoggerName("test.accesslog");

		// This is the format for each line. A number of substitution variables may
		// be used here. See the JavaDoc for LoggingInterceptor for information on
		// what is available.
		loggingInterceptor.setMessageFormat(
				"Source[${remoteAddr}] Operation[${operationType} ${idOrResourceName}] UA[${requestHeader.user-agent}] Params[${requestParameters}]");

		BusinessExceptionInterceptor simpleServerLoggingInterceptor = new BusinessExceptionInterceptor();
		registerInterceptor(simpleServerLoggingInterceptor);
	}

}