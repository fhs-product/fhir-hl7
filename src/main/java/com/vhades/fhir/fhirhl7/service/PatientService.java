package com.vhades.fhir.fhirhl7.service;

import com.vhades.fhir.fhirhl7.domain.resource.PatientRs;

import java.util.UUID;


public interface PatientService {
	PatientRs create(PatientRs patient);

	PatientRs findById(UUID id);
}
