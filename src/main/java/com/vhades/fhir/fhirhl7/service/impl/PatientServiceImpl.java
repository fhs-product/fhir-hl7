package com.vhades.fhir.fhirhl7.service.impl;

import com.vhades.fhir.fhirhl7.domain.PatientEntity;
import com.vhades.fhir.fhirhl7.domain.resource.PatientRs;
import com.vhades.fhir.fhirhl7.repository.PatientRepository;
import com.vhades.fhir.fhirhl7.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service("patientService")
public class PatientServiceImpl implements PatientService {

	@Autowired
	private PatientRepository patientRepository;

	@Override
	public PatientRs create(final PatientRs patient) {

		PatientEntity entity = PatientEntity.builder()
				.id(patient.getId())
				.createdBy(null)
				.modifiedBy(null)
				.resource(patient).build();

		entity = patientRepository.save(entity);

		return entity.getResource();
	}

	@Override
	public PatientRs findById(final UUID id) {
		PatientEntity entity = patientRepository.findById(id).orElseThrow();
		return entity.getResource();
	}

}
