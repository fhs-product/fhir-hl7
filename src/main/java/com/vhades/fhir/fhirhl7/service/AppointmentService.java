package com.vhades.fhir.fhirhl7.service;

import com.vhades.fhir.fhirhl7.domain.resource.AppointmentRs;

import java.util.List;
import java.util.UUID;

public interface AppointmentService {
	AppointmentRs create(AppointmentRs appointment);

	AppointmentRs update(UUID id, AppointmentRs appointment);

	AppointmentRs findById(UUID id);

	List<AppointmentRs> findAll();

	List<AppointmentRs> findByActorDisplayName(String actorDisplayName);

	AppointmentRs findByHistory(UUID id, Long versionId);
}
