package com.vhades.fhir.fhirhl7.service;

import com.vhades.fhir.fhirhl7.domain.resource.SlotRs;

import java.util.UUID;

public interface SlotService {
	SlotRs create(SlotRs slot);

	SlotRs findById(UUID id);
}
