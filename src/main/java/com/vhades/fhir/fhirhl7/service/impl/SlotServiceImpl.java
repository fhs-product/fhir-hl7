package com.vhades.fhir.fhirhl7.service.impl;

import com.vhades.fhir.fhirhl7.domain.SlotEntity;
import com.vhades.fhir.fhirhl7.domain.resource.SlotRs;
import com.vhades.fhir.fhirhl7.repository.SlotRepository;
import com.vhades.fhir.fhirhl7.service.SlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("slotService")
public class SlotServiceImpl implements SlotService {

	@Autowired
	private SlotRepository slotRepository;

	@Override
	public SlotRs create(final SlotRs slot) {

		SlotEntity entity = SlotEntity.builder()
				.id(slot.getId())
				.createdBy(null)
				.modifiedBy(null)
				.resource(slot).build();

		entity = slotRepository.save(entity);

		return entity.getResource();
	}

	@Override
	public SlotRs findById(final UUID id) {
		SlotEntity entity = slotRepository.findById(id).orElse(null);
		return entity == null ? null : entity.getResource();
	}

}
