package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.SlotRs;
import org.hl7.fhir.r4.model.Slot;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValueMappingStrategy;

import java.util.List;

@Mapper(
		componentModel = "spring",
		nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
		nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
		uses = {
				MetaMapper.class,
				NarrativeMapper.class,
				IdentifierMapper.class,
				CodeableConceptMapper.class,
				ReferenceMapper.class
		}
		)
public interface SlotMapper {

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "text.div", ignore = true)
	SlotRs toResource(Slot model);

	@Mapping(target = "text.div", ignore = true)
	@Mapping(expression = "java( String.valueOf(resource.getId() ))", target = "id")
	Slot toModel(SlotRs resource);

	List<Slot> toModels(List<SlotRs> resources);
}
