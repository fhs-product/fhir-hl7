package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.PeriodRs;
import org.hl7.fhir.r4.model.Period;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PeriodMapper {
	PeriodRs toResource(Period model);
	Period toModel(PeriodRs resource);

	List<PeriodRs> toResources(List<Period> model);
	List<Period> toModels(List<PeriodRs> resources);
}
