package com.vhades.fhir.fhirhl7.controller.resource.provider;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.vhades.fhir.fhirhl7.controller.mapper.PatientMapper;
import com.vhades.fhir.fhirhl7.domain.resource.PatientRs;
import com.vhades.fhir.fhirhl7.service.PatientService;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Enumerations.ResourceType;
import org.hl7.fhir.r4.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PatientResourceProvider implements IResourceProvider {

	@Autowired
	private PatientService patientService;

	@Autowired
	private PatientMapper patientMapper;

	@Override
	public Class<? extends IBaseResource> getResourceType() {
		return Patient.class;
	}

	@Create
	public MethodOutcome create(@ResourceParam Patient patient) throws Exception {

		PatientRs patientRs = patientService.create(patientMapper.toResource(patient));

		// Build response containing the new resource id
		MethodOutcome methodOutcome = new MethodOutcome();
		methodOutcome.setId(new IdDt(ResourceType.SLOT.getDisplay(), patientRs.getId().toString()));
		methodOutcome.setResource(patientMapper.toModel(patientRs));
		methodOutcome.setCreated(Boolean.TRUE);

		return methodOutcome;
	}
}