package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.MetaRs;
import org.hl7.fhir.r4.model.Meta;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;

@Mapper(
	componentModel = "spring", 
	nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
	uses = { CodingMapper.class }
)
public interface MetaMapper {
	@Mapping(target = "lastUpdated", ignore = true)
	MetaRs toResource(Meta model);

	@Mapping(source = "version", target = "versionId")
	Meta toModel(MetaRs resource);
}
