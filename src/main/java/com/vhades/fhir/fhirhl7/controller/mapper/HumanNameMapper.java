package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.HumanNameRs;
import org.hl7.fhir.r4.model.HumanName;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper(
	componentModel = "spring", 
	uses = {
		PeriodMapper.class,
		StringTypeMapper.class
	}
)
public interface HumanNameMapper {
	
	@Mapping(source = "model.text", target = "text")
	@Mapping(source = "model.given", target = "given", qualifiedByName = "toStringList")
	@Mapping(source = "model.prefix", target = "prefix", qualifiedByName = "toStringList")
	@Mapping(source = "model.suffix", target = "suffix", qualifiedByName = "toStringList")
	HumanNameRs toResource(HumanName model);
	
	@Mapping(source = "resource.given", target = "given", qualifiedByName = "toStringTypeList")
	@Mapping(source = "resource.prefix", target = "prefix", qualifiedByName = "toStringTypeList")
	@Mapping(source = "resource.suffix", target = "suffix", qualifiedByName = "toStringTypeList")
	HumanName toModel(HumanNameRs resource);

	List<HumanNameRs> toResources(List<HumanName> dtos);
	List<HumanName> toModels(List<HumanNameRs> resources);

}
