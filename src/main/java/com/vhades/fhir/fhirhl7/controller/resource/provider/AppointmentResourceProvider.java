package com.vhades.fhir.fhirhl7.controller.resource.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.util.BundleBuilder;
import com.vhades.fhir.fhirhl7.controller.mapper.AppointmentMapper;
import com.vhades.fhir.fhirhl7.controller.mapper.SlotMapper;
import com.vhades.fhir.fhirhl7.domain.resource.AppointmentRs;
import com.vhades.fhir.fhirhl7.domain.resource.SlotRs;
import com.vhades.fhir.fhirhl7.service.AppointmentService;
import com.vhades.fhir.fhirhl7.service.SlotService;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.instance.model.api.IBase;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Appointment;
import org.hl7.fhir.r4.model.Enumerations.ResourceType;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Component
public class AppointmentResourceProvider implements IResourceProvider {

    private final Logger LOGGER = LoggerFactory.getLogger(AppointmentResourceProvider.class);

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private SlotService slotService;
    @Autowired
    private SlotMapper slotMapper;
    @Autowired
    private AppointmentMapper appointmentMapper;
    private FhirContext fhirContext = FhirContext.forCached(FhirVersionEnum.R4);

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Appointment.class;
    }

    @Search()
    public List<Appointment> searchAllAppointments() {
        List<AppointmentRs> appointmentRs = appointmentService.findAll();

        return appointmentMapper.toModels(appointmentRs);
    }

    @Search()
    public IBaseBundle getAppointmentInfo(@RequiredParam(name = Appointment.SP_RES_ID) StringParam appointmentId, @IncludeParam(allow = {"Appointment:slot"})
            Set<Include> theIncludes) {
        BundleBuilder bundleBuilder = new BundleBuilder(fhirContext);
        bundleBuilder
                .setBundleField("type", "searchset")
                .setBundleField("id", UUID.randomUUID().toString())
                .setMetaField("lastUpdated", bundleBuilder.newPrimitive("instant", new Date()));
        AppointmentRs appointment = appointmentService.findById(UUID.fromString(appointmentId.getValue()));
        if (theIncludes.contains(new Include("Appointment:slot"))) {
            IBase appointmentEntry = bundleBuilder.addEntry();
            bundleBuilder.addToEntry(appointmentEntry, "resource", appointmentMapper.toModel(appointment));
            for (Reference slot : appointmentMapper.toModel(appointment).getSlot()) {
                IBase slotEntry = bundleBuilder.addEntry();
                SlotRs slotRs = slotService.findById(UUID.fromString(StringUtils.substringAfterLast(slot.getReference(), "/")));
                bundleBuilder.addToEntry(slotEntry, "resource", slotMapper.toModel(slotRs));
            }
        }
        return bundleBuilder.getBundle();
    }

    @Search
    public List<Appointment> getAppointmentsByParticipant(@RequiredParam(name = "actorName") final String actorName) {
        List<AppointmentRs> appointments = appointmentService.findByActorDisplayName(actorName);

        return appointmentMapper.toModels(appointments);
    }

    @Read(version = true)
    public Appointment read(@IdParam final IdType theId) {
        AppointmentRs appointmentRs = null;

        UUID id = UUID.fromString(theId.getIdPart());

        if (theId.hasVersionIdPart()) {
            appointmentRs = appointmentService.findByHistory(id, theId.getVersionIdPartAsLong());
        } else {
            appointmentRs = appointmentService.findById(id);
        }

        return appointmentMapper.toModel(appointmentRs);
    }

    @Create
    public MethodOutcome createAppointment(@ResourceParam final Appointment appointment) throws Exception {

        // Slots must existed
        //		for (Reference slot : appointment.getSlot()) {
        //			if (ResourceType.SLOT.getDisplay().equals(slot.getReferenceElement().getResourceType())) {
        //				String slotId = slot.getReferenceElement().getIdPart();
        //				SlotRs slotRs = slotService.findById(UUID.fromString(slotId));
        //				if (slotRs == null) {
        //					throw new UnprocessableEntityException(String.format("Slot resource reference value %s is not a valid resource.", slotId));
        //				}
        //			}
        //		}

        // convert dto and call service to create appointment
        AppointmentRs appointmentRs = appointmentService.create(appointmentMapper.toResource(appointment));

        // Build response containing the new resource id
        MethodOutcome methodOutcome = new MethodOutcome();
        methodOutcome.setId(new IdDt(ResourceType.APPOINTMENT.getDisplay(), appointmentRs.getId().toString()));
        methodOutcome.setResource(appointmentMapper.toModel(appointmentRs));
        methodOutcome.setCreated(Boolean.TRUE);

        return methodOutcome;
    }

    @Update
    public MethodOutcome updateAppointment(@IdParam final IdType appointmentId, @ResourceParam final Appointment appointment, final HttpServletRequest theRequest) {

        UUID id = UUID.fromString(appointmentId.getIdPart());
        AppointmentRs appointmentRs = appointmentService.update(id, appointmentMapper.toResource(appointment));

        MethodOutcome methodOutcome = new MethodOutcome();
        methodOutcome.setId(new IdDt(ResourceType.APPOINTMENT.getDisplay(), appointmentRs.getId().toString()));
        methodOutcome.setResource(appointmentMapper.toModel(appointmentRs));
        methodOutcome.setCreated(Boolean.TRUE);

        return methodOutcome;
    }
}