package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.ParticipantRs;
import org.hl7.fhir.r4.model.Appointment.AppointmentParticipantComponent;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
	componentModel = "spring", 
	uses = {
		CodeableConceptMapper.class, 
		ReferenceMapper.class,
		PeriodMapper.class
	}
)
public interface ParticipantMapper {
	ParticipantRs toResource(AppointmentParticipantComponent model);
	AppointmentParticipantComponent toModel(ParticipantRs resource);

	List<ParticipantRs> toResources(List<AppointmentParticipantComponent> models);
	List<AppointmentParticipantComponent> toModels(List<AppointmentParticipantComponent> resources);
}
