package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.IdentifierRs;
import org.hl7.fhir.r4.model.Identifier;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
	componentModel = "spring", 
	uses = {
		CodeableConceptMapper.class, 
		PeriodMapper.class
	}
)
public interface IdentifierMapper {
	IdentifierRs toResource(Identifier model);
	Identifier toModel(IdentifierRs resource);

	List<IdentifierRs> toResources(List<Identifier> dtos);
	List<Identifier> toModels(List<IdentifierRs> resources);
}
