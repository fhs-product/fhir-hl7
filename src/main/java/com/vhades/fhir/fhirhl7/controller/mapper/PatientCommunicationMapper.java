package com.vhades.fhir.fhirhl7.controller.mapper;//package com.fhir.example.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.PatientCommunicationRs;
import org.hl7.fhir.r4.model.Patient;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
	componentModel = "spring",
	uses = {
		CodeableConceptMapper.class
	}
)
public interface PatientCommunicationMapper {

	PatientCommunicationRs toResource(Patient.PatientCommunicationComponent model);
	Patient.PatientCommunicationComponent toModel(PatientCommunicationRs resource);

	List<PatientCommunicationRs> toResources(List<Patient.PatientCommunicationComponent> dtos);
	List<Patient.PatientCommunicationComponent> toModels(List<PatientCommunicationRs> resources);
}
