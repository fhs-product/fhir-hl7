package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.CodingRs;
import org.hl7.fhir.r4.model.Coding;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CodingMapper {
	CodingRs toResource(Coding model);
	Coding toModel(CodingRs resource);

	List<CodingRs> toResources(List<Coding> models);
	List<Coding> toModels(List<CodingRs> resources);
}
