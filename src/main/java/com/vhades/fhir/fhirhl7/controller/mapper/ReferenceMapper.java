package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.ReferenceRs;
import org.hl7.fhir.r4.model.Reference;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
	componentModel = "spring", 
	uses = {
		IdentifierMapper.class
	}
)
public interface ReferenceMapper {
	ReferenceRs toResource(Reference model);
	Reference toModel(ReferenceRs resource);
	
	List<ReferenceRs> toResources(List<Reference> models);
	List<Reference> toModels(List<ReferenceRs> resources);
}
