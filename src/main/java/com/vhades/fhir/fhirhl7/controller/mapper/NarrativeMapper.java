package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.NarrativeRs;
import org.hl7.fhir.r4.model.Narrative;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface NarrativeMapper {
	
	@Mapping(target = "div", ignore = true)
	NarrativeRs toResource(Narrative model);

	@Mapping(target = "div", ignore = true)
	Narrative toModel(NarrativeRs resource);
}
