package com.vhades.fhir.fhirhl7.controller.mapper;

import org.hl7.fhir.r4.model.Resource;
import org.mapstruct.Named;

import java.util.UUID;

public interface BaseMapper {
	
	@Named("idTypeToUuid") 
    public static UUID idTypeToUuid(Resource model) {
		if (model.hasId()) {
			return UUID.fromString(model.getIdElement().getIdPart()); 
		}
		return UUID.randomUUID();
    }

	@Named("uuidToString") 
	public static String uuidToString(UUID id) { 
		return id.toString();
	}
	
}
