package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.CodeableConceptRs;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CodingMapper.class})
public interface CodeableConceptMapper {
	CodeableConceptRs toResource(CodeableConcept model);
	CodeableConcept toModel(CodeableConceptRs resource);

	List<CodeableConceptRs> toResources(List<CodeableConcept> models);
	List<CodeableConcept> toModels(List<CodeableConceptRs> resources);
}
