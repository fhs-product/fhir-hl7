package com.vhades.fhir.fhirhl7.controller.mapper;//package com.fhir.example.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.ContactRs;
import org.hl7.fhir.r4.model.Patient.ContactComponent;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
	componentModel = "spring",
	uses = {
		CodeableConceptMapper.class,
		HumanNameMapper.class,
		StringTypeMapper.class,
        ContactPointMapper.class,
        AddressMapper.class,
        ReferenceMapper.class,
        PeriodMapper.class
	}
)
public interface ContactMapper {

	ContactRs toResource(ContactComponent model);
	ContactComponent toModel(ContactRs resource);

	List<ContactRs> toResources(List<ContactComponent> dtos);
	List<ContactComponent> toModels(List<ContactRs> resources);
}
