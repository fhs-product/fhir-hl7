package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.ContactPointRs;
import org.hl7.fhir.r4.model.ContactPoint;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ContactPointMapper {

	ContactPointRs toResource(ContactPoint model);

	ContactPoint toModel(ContactPointRs resource);

	List<ContactPointRs> toResources(List<ContactPoint> dtos);

	List<ContactPoint> toModels(List<ContactPointRs> resources);
}
