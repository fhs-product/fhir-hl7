package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.PatientRs;
import org.hl7.fhir.r4.model.*;
import org.mapstruct.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Mapper(
	componentModel = "spring",
	nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
	nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
	uses = {
			NarrativeMapper.class,
			MetaMapper.class,
			IdentifierMapper.class,
			HumanNameMapper.class,
			ContactPointMapper.class,
			AddressMapper.class,
			CodeableConceptMapper.class,
			AttachmentMapper.class,
			ContactMapper.class,
			PatientCommunicationMapper.class,
			ReferenceMapper.class,
			PatientLinkMapper.class
	}
)
public interface PatientMapper extends BaseMapper {

	@Mapping(source = "model", target = "id", qualifiedByName = "idTypeToUuid")
	@Mapping(source = "model", target = "deceasedBoolean", qualifiedByName = "getDeceasedBoolean")
	@Mapping(source = "model", target = "deceasedDateTime", qualifiedByName = "getDeceasedDateTime")
	@Mapping(source = "model", target = "multipleBirthBoolean", qualifiedByName = "getMultipleBirthBoolean")
	@Mapping(source = "model", target = "multipleBirthInteger", qualifiedByName = "getMultipleBirthInteger")
	PatientRs toResource(Patient model);

	@Mapping(source = "id", target = "id", qualifiedByName = "uuidToString")
	@Mapping(source = "resource", target = "deceased", qualifiedByName = "toDeceased")
	@Mapping(source = "resource", target = "multipleBirth", qualifiedByName = "toMultipleBirth")
	Patient toModel(PatientRs resource);

	List<Patient> toModels(List<PatientRs> resources);
	
	@Named("toDeceased")
	public static Type toDeceased(PatientRs resource) {
		if (resource.getDeceasedBoolean() != null) {
			return new BooleanType(resource.getDeceasedBoolean());
		} else if (resource.getDeceasedDateTime() != null) {
			DateTimeFormatter hapiDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
			return DateTimeType.parseV3(resource.getDeceasedDateTime().format(hapiDateTimeFormatter));
		}
		return null;
	}

	@Named("getDeceasedBoolean")
	public static Boolean getDeceasedBoolean(Patient model) {
		return model.hasDeceasedBooleanType() ? model.getDeceasedBooleanType().booleanValue() : null;
	}
	
	@Named("getDeceasedDateTime") 
	public static LocalDateTime getDeceasedDateTime(Patient model) {
		if (model.hasDeceasedDateTimeType()) {
			DateTimeType deceasedDateTime =  model.getDeceasedDateTimeType();
			return LocalDateTime.of(deceasedDateTime.getYear(), deceasedDateTime.getMonth(), deceasedDateTime.getDay(), deceasedDateTime.getHour(), deceasedDateTime.getMinute());
		}
		return null;
	}
	
	@Named("getMultipleBirthBoolean")
	public static Boolean getMultipleBirthBoolean(Patient model) {
		return model.hasMultipleBirthBooleanType() ? model.getMultipleBirthBooleanType().booleanValue() : null;
	}
	
	@Named("getMultipleBirthInteger") 
	public static Integer getMultipleBirthInteger(Patient model) {
		return model.hasMultipleBirthIntegerType() ? model.getMultipleBirthIntegerType().getValue() : null;
	}
	
	
	@Named("toMultipleBirth")
	public static Type toMultipleBirth(PatientRs resource) {
		if (resource.getMultipleBirthBoolean() != null) {
			return new BooleanType(resource.getMultipleBirthBoolean());
		} else if (resource.getMultipleBirthInteger() != null) {
			return new IntegerType(resource.getMultipleBirthInteger());
		}
		return null;
	}
}
