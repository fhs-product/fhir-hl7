package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.AddressRs;
import org.hl7.fhir.r4.model.Address;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper(
	componentModel = "spring", 
	uses = { 
		StringTypeMapper.class,
		PeriodMapper.class
	}
)
public interface AddressMapper {

	@Mapping(source = "model.line", target = "line", qualifiedByName = "toStringList")
	AddressRs toResource(Address model);

	@Mapping(source = "resource.line", target = "line", qualifiedByName = "toStringTypeList")
	Address toModel(AddressRs resource);

	List<AddressRs> toResources(List<Address> dtos);

	List<Address> toModels(List<AddressRs> resources);
}
