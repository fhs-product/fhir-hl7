package com.vhades.fhir.fhirhl7.controller.mapper;

import org.hl7.fhir.r4.model.StringType;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

//@Mapper(componentModel = "spring")
public interface StringTypeMapper {
	
	@Named("toStringList") 
	public static <T> List<String> toStringList(List<T> stringTypes) { 
		return stringTypes.stream().map(Object::toString).collect(Collectors.toList());
	}
	
	@Named("toStringTypeList") 
	public static List<StringType> toStringTypeList(List<String> strList) { 
		return strList.stream().map(s -> new StringType(s)).collect(Collectors.toList());
	}
}
