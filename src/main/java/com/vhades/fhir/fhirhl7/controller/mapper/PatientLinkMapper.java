package com.vhades.fhir.fhirhl7.controller.mapper;//package com.fhir.example.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.PatientLinkRs;
import org.hl7.fhir.r4.model.Patient;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
	componentModel = "spring",
	uses = {
        ReferenceMapper.class
	}
)
public interface PatientLinkMapper {

	PatientLinkRs toResource(Patient.PatientLinkComponent model);
	Patient.PatientLinkComponent toModel(PatientLinkRs resource);

	List<PatientLinkRs> toResources(List<Patient.PatientLinkComponent> dtos);
	List<Patient.PatientLinkComponent> toModels(List<PatientLinkRs> resources);
}
