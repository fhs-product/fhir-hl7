package com.vhades.fhir.fhirhl7.controller.mapper;

import com.vhades.fhir.fhirhl7.domain.resource.AttachmentRs;
import org.hl7.fhir.r4.model.Attachment;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface AttachmentMapper {

	AttachmentRs toResource(Attachment model);

	Attachment toModel(AttachmentRs resource);

	List<AttachmentRs> toResources(List<Attachment> dtos);

	List<Attachment> toModels(List<AttachmentRs> resources);
}
