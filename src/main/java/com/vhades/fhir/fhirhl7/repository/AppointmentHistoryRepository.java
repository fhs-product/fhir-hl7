package com.vhades.fhir.fhirhl7.repository;

import com.vhades.fhir.fhirhl7.domain.AppointmentHistoryEntity;
import com.vhades.fhir.fhirhl7.domain.HistoryEntityId;
import org.springframework.data.repository.CrudRepository;

public interface AppointmentHistoryRepository extends CrudRepository<AppointmentHistoryEntity, HistoryEntityId> {

	// select * from appointment_history where resource @> '{"id" : "b93fdb6d-652a-4b04-a49a-75c7dfc652cb", "meta" : {"versionId" : 1} }';
}
