package com.vhades.fhir.fhirhl7.repository;

import com.vhades.fhir.fhirhl7.domain.SlotEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface SlotRepository extends CrudRepository<SlotEntity, UUID> {

}
