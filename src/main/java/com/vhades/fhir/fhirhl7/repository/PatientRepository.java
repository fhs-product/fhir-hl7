package com.vhades.fhir.fhirhl7.repository;

import com.vhades.fhir.fhirhl7.domain.PatientEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;


public interface PatientRepository extends CrudRepository<PatientEntity, UUID> {

}
