#!/bin/bash

if [[ -z $1 ]] || [[ -z $2 ]]; then
  echo """
HOW TO RUN:

  ./builddocker.sh <service_name> <project> <path_to_pom_file>

Example:

  ./builddocker.sh iam-service fhs-product .
  """
    
  exit 1
fi


service_name=$1
project=$2
image_name="asia.gcr.io/${project}/${service_name}"
path_to_pom=$3

cd ${path_to_pom}
mvn clean install -DskipTests
docker build --build-arg service_name=${service_name} -t ${image_name}:0.1.0 .
docker push ${image_name}:0.1.0
